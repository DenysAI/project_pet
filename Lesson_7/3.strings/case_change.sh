#!/bin/bash

string="Hello, World!"

# Преобразование строки в верхний регистр
upper_case=${string^^}

echo "Строка в верхнем регистре: \"$upper_case\""

# Преобразование строки в нижний регистр
lower_case=${string,,}

echo "Строка в нижнем регистре: \"$lower_case\""

