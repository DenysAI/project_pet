#!/bin/bash

# Функция, которая принимает два параметра
greet_user() {
    local name="$1"
    local greeting="$2"
    echo "$greeting, ${name}!"
}

# Передача параметров в функцию
greet_user "Привет" "Anna"

