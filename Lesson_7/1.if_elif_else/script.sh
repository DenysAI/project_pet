#!/bin/bash

while true; do
    echo "Введите вашу операционную систему (linux, mac или windows) или 'exit' для выхода:"
    read input

    os=$(echo "$input" | tr '[:upper:]' '[:lower:]')

    if [[ "$os" == "exit" ]]; then
        echo "Выход из программы."
        break
    elif [[ "$os" == "linux" ]]; then
        echo "Привет хакер!"
    elif [[ "$os" == "mac" || "$os" == "macos" ]]; then
        echo "Привет маковод!"
    elif [[ "$os" == "windows" ]]; then
        echo "Здесь нет никого."
    else
        echo "Неверный ввод. Пожалуйста, введите linux, mac, windows или 'exit'."
    fi
done

