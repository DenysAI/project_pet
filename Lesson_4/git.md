# Git

Git - это распределенная система управления версиями, которая используется для отслеживания изменений в коде проекта.

## Основные команды:

### 1. `git init`
   - Создает новый репозиторий Git в текущем каталоге.
   - Пример: `git init`

### 2. `git add`
   - Добавляет изменения в индекс для последующего коммита.
   - Пример: `git add filename.txt`

### 3. `git commit -m “message”`
   - Фиксирует текущие изменения в репозитории с сообщением о коммите.

### 4. `git log`
   - Показывает историю коммитов.
   - Пример: `git log`

### 5. `git status`
   - Показывает текущее состояние рабочего каталога и индекса.
   - Пример: `git status`

### 6. `git diff`
   - Показывает различия между рабочим каталогом и индексом.
   - Пример: `git diff`

### 7. `git diff --staged`
   - Показывает различия между индексом и последним коммитом.
   - Пример: `git diff --staged`

### 8. `git diff <filename>`
   - Показывает различия для указанного файла между рабочим каталогом и индексом.
   - Пример: `git diff filename.txt`

### 9. `git rm <filename>`
   - Удаляет указанный файл из индекса и рабочего каталога.
   - Пример: `git rm filename.txt`

### 10. `git mv <filename_1> <filename_2>`
   - Переименовывает файл в рабочем каталоге и сразу обновляет индекс.
   - Пример: `git mv old_filename.txt new_filename.txt`

### 11. `git checkout <something>`
   - Переключает ветку, восстанавливает файлы из индекса или изменяет их в рабочем каталоге.
   - Пример: `git checkout master`

### 12. `git reset HEAD <filename>`
   - Снимает файлы с индекса, но оставляет их неизмененными в рабочем каталоге.
   - Пример: `git reset HEAD filename.txt`

### 13. `git commit --amend -m “message”`
   - Добавляет изменения к последнему коммиту.
   - Пример: `git commit --amend -m "новое сообщение"`

### 14. `git checkout <hash> --filename`
   - Восстанавливает файл из определенного коммита.
   - Пример: `git checkout 1a2b3c4d --filename.txt`

### 15. `git ls-tree <tree-ish>`
   - Показывает содержимое объекта дерева.
   - Пример: `git ls-tree HEAD`

### 16. `git remote add <alias> <URL>`
   - Добавляет новый удаленный репозиторий.
   - Пример: `git remote add origin https://github.com/user/repo.git`

### 17. `git push <alias> <branch_name>`
   - Загружает локальные изменения в удаленный репозиторий.
   - Пример: `git push origin master`

### 18. `git fetch <remote_repo>`
   - Загружает объекты и ссылки из удаленного репозитория.
   - Пример: `git fetch origin`

### 19. `git merge <branch>`
   - Объединяет указанную ветку с текущей.
   - Пример: `git merge feature_branch`

### 20. `git commit -a -m ‘commit message’`
   - Добавляет и фиксирует все измененные файлы.
   - Пример: `git commit -a -m "commit message"`

### 21. `git log --oneline`
   - Выводит историю коммитов в одну строку.
   - Пример: `git log --oneline`

### 22. `git diff <hash>`
   - Показывает различия между двумя коммитами.
   - Пример: `git diff abc123 def456`

### 23. `git branch -m <old_branch_name> <new_branch_name>`
   - Переименовывает ветку.
   - Пример: `git branch -m old_branch new_branch`

### 24. `git branch -d`
   - Удаляет указанную ветку.
   - Пример: `git branch -d branch_to_delete`

### 25. `git branch -D`
   - Принудительно удаляет указанную ветку.
   - Пример: `git branch -D branch_to_delete`

### 26. `git tag <tag_name>`
   - Создает тег для текущего коммита.
   - Пример: `git tag v1.0`

### 27. `git tag -a <tag_name> -m “message”`
   - Создает аннотированный тег с сообщением.
   - Пример: `git tag -a v1.0 -m "Release version 1.0"`

### 28. `git show <tag_name>`
   - Показывает информацию о коммите, на который указывает тег.
   - Пример: `git show v1.0`

### 29. `git push origin --tags`
   - Отправить теги на удаленный репозиторий.
   - Пример: `git push origin --tags`

