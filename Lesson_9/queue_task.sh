#!/bin/bash
#set -x

# Устанавливаем пустой массив для хранения PIDs
pids=()

# Удаляем существующий и создаем новый FIFO
mkfifo task_queue

# Запуск чтения из FIFO в фоновом режиме
while true; do
    if read -r task < task_queue; then
        echo "Processing task: $task"
    else
        break
    fi
done &

# Добавление задач в FIFO и сохранение их PIDs
echo "Task 1" > task_queue &
pids+=($!)  # Сохраняем PID первой задачи
sleep 0.1
echo "Task 2" > task_queue &
pids+=($!)  # Сохраняем PID второй задачи

# Ожидание завершения обеих задач
wait "${pids[@]}"

# Удаление FIFO
rm -rf task_queue

# Завершение программы
#exit 0
