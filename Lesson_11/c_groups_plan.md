systemd-cgtop

systemctl show user-1000.slice | grep MemoryLimit

systemctl set-property user-1000.slice MemoryLimit=500M

sudo systemd-run --slice=my.slice /home/ubuntu/test_slice.sh

systemctl list-units --state=failed

sudo systemctl disable run-re9c519e0ee44429b92cd9b553173d4b0.service

sudo systemctl reset-failed run-re9c519e0ee44429b92cd9b553173d4b0.service

sudo systemctl daemon-reload

sudo systemctl restart wds.service

sudo systemctl status wds.service

systemctl show wds.service | grep -E 'MemoryLimit|BlockIOWeight'

systemctl status

systemd-cgtop

cat /proc/25378/cgroup

mount | grep cgroup

ls /sys/fs/cgroup/
