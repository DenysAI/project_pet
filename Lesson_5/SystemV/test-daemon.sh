!/bin/bash
# description: Foo server

# Start the service FOO
start() {
    if [ -f /var/lock/subsys/testrun ]; then
        echo "Service is already running."
        exit 1
    fi
    /usr/sbin/test-run.sh &
    ### Create the lock file ###
    touch /var/lock/subsys/testrun
}


# Stop the service FOO
stop() {
        for pid in $(ps aux | grep test-run.sh | awk -r '{print $2}'); do kill $pid; done
        ### Now, delete the lock file ###
        rm -f /var/lock/subsys/testrun
        echo
}

### main logic ###
case "$1" in
  start)
        start
        ;;
  stop)
        stop
        ;;
  status)
        status FOO
        ;;
  restart|reload|condrestart)
        stop
        start
        ;;
  *)
        echo $"Usage: $0 {start|stop|restart|reload|status}"
        exit 1
esac

exit 0

