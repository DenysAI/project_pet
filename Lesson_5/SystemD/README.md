### Copy to server
```sh
sudo cp test-systemd.sh /usr/sbin/
sudo cp test.service /etc/systemd/system/test.service
```

### Enable it
```sh
sudo systemctl daemon-reload
sudo systemctl enable test.service
sudo systemctl start test.service
```
