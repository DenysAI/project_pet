#!/bin/bash

if [ $# -eq 1 ]; then
	if dpkg -l | awk '{print $2}' | tail -n +5 | grep -wqi ${1}; then
	      echo "The package ${1} is already installed"
        else
		if apt search $1 2> /dev/null | grep -wqi $1 ; then
	          echo "Installing ${1}..........."	
                  sudo apt install ${1}
	        else
		  echo "There is no package $1 in the apt repository"
	        fi
	fi
else 
	echo "The command should be run as: $0 <package name>"
fi	
