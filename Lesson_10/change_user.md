#### Add users and groups
sudo useradd -m -d /opt/testuser testuser
sudo usermod -l newUsername oldUsername
sudo groupmod -n newUsername oldUsername
sudo usermod -d /home/newHomeDir -m newUsername

#### Set password
sudo passwd username

#### User info
id

#### Set shell
chsh -s /bin/bash
sudo usermod -s /bin/bash john

sudo usermod -u 1001 username
sudo groupmod -g 1001 username

sudo groupadd examplegroup
sudo usermod -aG examplegroup exampleuser

sudo chattr +i pivo


#### Commands: `useradd`, `passwd`, `userdel`

- `useradd`: Создание нового пользователя.

  ```asp
  sudo useradd -m -s /bin/bash new_user
  ```

- `passwd`: Установка пароля для пользователя.

  ```asp
  sudo passwd new_user
  ```

- `userdel`: Удаление пользователя.
  ```asp
  sudo userdel old_user
  ```
